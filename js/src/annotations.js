const annotations = [
	{
		xPosition: "2015-02-01", // x-Wert der Annotation, bei Karten lon
		yPosition: 47.791407, // y-Wert der Annotation, bei Karten lat
		distance: 25, // länge des Striches der Annotation
		direction: "top", // top,right,bottom,left - in welche Richtung soll die Annotation gehen?
		text: "Bodenseekreis", // Trennungen mit <br>
		mobile: true, // Soll die Annotation mobil angeteigt werden?
		desktop: true, // Soll die Annotation auf dem Desktop angeteigt werden?
		class: "___anno_cssKlasse",
	},
];

export default annotations;
