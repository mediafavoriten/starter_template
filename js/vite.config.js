import svg from "vite-plugin-svgstring";
import dsv from "@rollup/plugin-dsv";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";
import gzipPlugin from "rollup-plugin-gzip";

const config = {
	base: "",
	plugins: [svelte(), dsv(), svg(), cssInjectedByJsPlugin()],
	esbuild: {
		drop: ["console", "debugger"],
	},
	envDir: "envs/",
	build: {
		chunkSizeWarningLimit: 500,
		rollupOptions: {
			external: ["d3", "leaflet"],
			// plugins: [gzipPlugin()],
			output: {
				dir: "dist/",
				compact: true,
				entryFileNames: "pfas_region.js",
				assetFileNames: "index.css",
				chunkFileNames: "chunk.js",
				inlineDynamicImports: true,
				format: "iife",
				globals: {
					d3: "d3",
					L: "leaflet",
				},
			},
		},
	},
};

export default config;
