# Titel der Analyse bzw. des Projekts

Hier steht ggf. eine kurze Beschreibung

## Analysefragen

-   Welche Fragen
-   stellt das
-   Projekt?

## Datenquellen

-   Alle benutzte Quellen
-   [idealerweise mit Links](https://www.suedkurier.de)

## Skripte

Kurze Beschreibung der Skripte und wie sie ggf. zusammenarbeiten

## Sonstiges

Alles, was sonst noch wichtig ist

## Veröffentlichung

Wann und wo und unter welchem Link wurde das Projekt veröffentlicht.
